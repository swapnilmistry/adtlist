package com.example.ADTList;

public class Main {
    public static void main(String[] args) {
       try {
           String[] items = {"Milk", "Eggs", "Celery", "Bananas", "Apples", "Oranges", "Cookies", "Steak"};
           CustomList myCustomList = new CustomList<>(items.length);
           for (int i = 0; i < items.length; i++) {
               // Insert each item into list and print list after each insertion.
               myCustomList.add(items[i], i);
               System.out.print("List after inserting " +items[i]+ ": ");
               System.out.println(myCustomList.toString());
           }

           System.out.println();

           // Remove Celery from list
           myCustomList.remove(2);
           System.out.println("List after removing Celery: " +myCustomList.toString());
           // Remove Oranges from list
           myCustomList.remove(4);
           System.out.println("List after removing Orange: " +myCustomList.toString());

           System.out.println();

           // Swap items at index 1 and 3 => This are valid indexes
           myCustomList.swap(1, 3);
           System.out.println("List after swapping items at index 1 and 3: " +myCustomList.toString());

           System.out.println();
           // Swap items at index 10 and 3 => These are invalid indexes
           // Prints error message from catch block
           myCustomList.swap(10, 3);

       } catch (Exception e) {
           System.out.println(e.getMessage());
       }
    }
}
