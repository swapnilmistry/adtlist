package com.example.ADTList;

public class CustomListException extends Exception {
    public CustomListException(String message) {
        super(message);
    }
}
