package com.example.ADTList;

public interface CustomListInterface<T> {
    public boolean isEmpty();
    public int size();
    public void add(T item, int i) throws CustomListException;
    public T remove(int i) throws CustomListException;
    public void removeAll();
    public T get(int i) throws CustomListException;
}
