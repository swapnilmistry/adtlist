package com.example.ADTList;

public class CustomList<T> implements CustomListInterface<T> {
    private T listArray[];
    private int maxSize;
    private int currentSize;

    public CustomList(int maxSize) {
        this.listArray = (T[]) new Object[maxSize];
        this.maxSize = maxSize;
        this.currentSize = 0;
    }

    @Override
    public boolean isEmpty() {
        if (this.currentSize == 0) {
            return true;
        }
        return false;
    }

    @Override
    public int size() {
        return this.currentSize;
    }

    @Override
    public void add(T item, int i) throws CustomListException {
        if (!validateIndex(i)) {
            throw new CustomListException("Cannot add element at index " + i + ". List index out of bound.");
        }

        if (this.listArray[i] != null) {
            throw new CustomListException("Cannot add element. Element " + this.listArray[i] + " already present at index " + i);
        }

        this.listArray[i] = item;
        this.currentSize = this.currentSize + 1;

    }

    @Override
    public T remove(int i) throws CustomListException {
        if (!validateIndex(i)) {
            throw new CustomListException("Cannot remove element from index " + i + ". List index out of bound.");
        }
        T elementToRemove = this.listArray[i];
        for (int j = i; j < listArray.length - 1; j++) {
                this.listArray[j] = this.listArray[j+1];
        }

        this.currentSize = this.currentSize - 1;
        this.maxSize = this.maxSize - 1;
        return elementToRemove;
    }

    @Override
    public void removeAll() {
        this.listArray = (T[]) new Object[0];
        this.currentSize = 0;
    }

    @Override
    public T get(int i) throws CustomListException {
        if (!validateIndex(i)) {
            throw new CustomListException("Cannot get element for index " + i + ". List index out of bound.");
        }
        return this.listArray[i];
    }

    private boolean validateIndex(int i) {
        if (i > this.maxSize - 1 || i < 0) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String arrayListItems = "";
        for (int i = 0; i < currentSize; i++) {
            arrayListItems = arrayListItems + this.listArray[i];
            if (i != currentSize - 1) {
                arrayListItems = arrayListItems + ", ";
            }
        }
        return arrayListItems;
    }

    public void swap(int i, int j) throws CustomListException {
        if (!validateIndex(i) || !this.validateIndex(j)) {
            throw new CustomListException("Cannot swap element for index. Invalid index provided.");
        }
        T temp = this.listArray[i];
        this.listArray[i] = this.listArray[j];
        this.listArray[j] = temp;
    }
}
